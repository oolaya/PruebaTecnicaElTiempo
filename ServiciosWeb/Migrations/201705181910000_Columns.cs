namespace ServiciosWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Columns : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Asignaturas",
                c => new
                    {
                        IdAsignatura = c.Int(nullable: false, identity: true),
                        nombre = c.String(),
                        IdPrograma = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IdAsignatura)
                .ForeignKey("dbo.ProgramaAcademicoes", t => t.IdPrograma, cascadeDelete: false)
                .Index(t => t.IdPrograma);

            CreateTable(
                "dbo.EstudiantesAsignaturas",
                c => new
                    {
                        IdEstudianteAsignatura = c.Int(nullable: false, identity: false),
                        cedula = c.Int(nullable: false),
                        IdAsignatura = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IdEstudianteAsignatura)
                .ForeignKey("dbo.Asignaturas", t => t.IdAsignatura, cascadeDelete: false)
                .ForeignKey("dbo.Estudiantes", t => t.cedula, cascadeDelete: false)
                .Index(t => t.cedula)
                .Index(t => t.IdAsignatura);

            CreateTable(
                "dbo.Estudiantes",
                c => new
                    {
                        cedula = c.Int(nullable: false, identity: false),
                        nombres = c.String(),
                        celular = c.String(),
                        IdPrograma = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.cedula)
                .ForeignKey("dbo.ProgramaAcademicoes", t => t.IdPrograma, cascadeDelete: false)
                .Index(t => t.IdPrograma);

            CreateTable(
                "dbo.ProgramaAcademicoes",
                c => new
                    {
                        IdPrograma = c.Int(nullable: false, identity: true),
                        nombre = c.String(),
                    })
                .PrimaryKey(t => t.IdPrograma);

            CreateTable(
                "dbo.Profesors",
                c => new
                    {
                        cedula = c.Int(nullable: false, identity: false),
                        nombres = c.String(),
                        IdPrograma = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.cedula)
                .ForeignKey("dbo.ProgramaAcademicoes", t => t.IdPrograma, cascadeDelete: false)
                .Index(t => t.IdPrograma);

            CreateTable(
                "dbo.profesorAsignaturas",
                c => new
                    {
                        IdprofesorAsignatura = c.Int(nullable: false, identity: true),
                        cedula = c.Int(nullable: false),
                        IdAsignatura = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IdprofesorAsignatura)
                .ForeignKey("dbo.Asignaturas", t => t.IdAsignatura, cascadeDelete: false)
                .ForeignKey("dbo.Profesors", t => t.cedula, cascadeDelete: false)
                .Index(t => t.cedula)
                .Index(t => t.IdAsignatura);

            CreateTable(
                "dbo.LogErrores",
                c => new
                    {
                        IdLogErrores = c.Int(nullable: false, identity: true),
                        Segmento = c.Int(),
                        FechaLogErrores = c.DateTime(nullable: false),
                        Controlador = c.String(),
                        ClaseLogError = c.String(),
                        MetodoLogError = c.String(),
                        LineaLogError = c.String(),
                        ColumnaLogError = c.String(),
                        MensajeLogError = c.String(),
                        HelpLink = c.String(),
                        HResult = c.String(),
                        InnerException = c.String(),
                        Source = c.String(),
                        StackTrace = c.String(),
                        TargetSite = c.String(),
                        ComentarioDesarrollador = c.String(),
                    })
                .PrimaryKey(t => t.IdLogErrores);
            
        }
        
        public override void Down()
        {
            //DropForeignKey("dbo.Profesors", "IdPrograma", "dbo.ProgramaAcademicoes");
            //DropForeignKey("dbo.profesorAsignaturas", "cedula", "dbo.Profesors");
            //DropForeignKey("dbo.profesorAsignaturas", "IdAsignatura", "dbo.Asignaturas");
            //DropForeignKey("dbo.Estudiantes", "IdPrograma", "dbo.ProgramaAcademicoes");
            //DropForeignKey("dbo.Asignaturas", "IdPrograma", "dbo.ProgramaAcademicoes");
            //DropForeignKey("dbo.EstudiantesAsignaturas", "cedula", "dbo.Estudiantes");
            //DropForeignKey("dbo.EstudiantesAsignaturas", "IdAsignatura", "dbo.Asignaturas");
            //DropIndex("dbo.profesorAsignaturas", new[] { "IdAsignatura" });
            //DropIndex("dbo.profesorAsignaturas", new[] { "cedula" });
            //DropIndex("dbo.Profesors", new[] { "IdPrograma" });
            //DropIndex("dbo.Estudiantes", new[] { "IdPrograma" });
            //DropIndex("dbo.EstudiantesAsignaturas", new[] { "IdAsignatura" });
            //DropIndex("dbo.EstudiantesAsignaturas", new[] { "cedula" });
            //DropIndex("dbo.Asignaturas", new[] { "IdPrograma" });
            //DropTable("dbo.LogErrores");
            //DropTable("dbo.profesorAsignaturas");
            //DropTable("dbo.Profesors");
            //DropTable("dbo.ProgramaAcademicoes");
            //DropTable("dbo.Estudiantes");
            //DropTable("dbo.EstudiantesAsignaturas");
            //DropTable("dbo.Asignaturas");
        }
    }
}
