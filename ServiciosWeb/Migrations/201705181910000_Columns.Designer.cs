// <auto-generated />
namespace ServiciosWeb.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.1-30610")]
    public sealed partial class Columns : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Columns));
        
        string IMigrationMetadata.Id
        {
            get { return "201705181910000_Columns"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
