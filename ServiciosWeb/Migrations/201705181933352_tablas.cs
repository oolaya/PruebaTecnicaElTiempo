namespace ServiciosWeb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tablas : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Notas",
                c => new
                    {
                        IdNota = c.Int(nullable: false, identity: true),
                        Nota = c.Double(nullable: false),
                        Corte = c.Int(nullable: false),
                        cedulaEstudiante = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.IdNota)
                .ForeignKey("dbo.Estudiantes", t => t.cedulaEstudiante, cascadeDelete: false)
                .Index(t => t.cedulaEstudiante);
            
        }
        
        public override void Down()
        {
            //DropForeignKey("dbo.Notas", "cedulaEstudiante", "dbo.Estudiantes");
            //DropIndex("dbo.Notas", new[] { "cedulaEstudiante" });
            //DropTable("dbo.Notas");
        }
    }
}
