﻿using ServiciosWeb.Models;
using ServiciosWeb.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace ServiciosWeb.WS
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IMaterias" in both code and config file together.
    [ServiceContract]
    public interface IMaterias
    {
        [OperationContract]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "GET", UriTemplate = "GetAsignaturas", BodyStyle = WebMessageBodyStyle.Bare)]
        List<ModelAsignatura> GetAsignaturas();

        [OperationContract]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "GET", UriTemplate = "GetAsignaturasxIdPrograma/{IdPrograma}", BodyStyle = WebMessageBodyStyle.Bare)]
        List<ModelAsignatura> GetAsignaturasxIdPrograma(string IdPrograma);



    }
}
