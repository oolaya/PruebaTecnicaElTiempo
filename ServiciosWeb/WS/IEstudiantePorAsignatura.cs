﻿using ServiciosWeb.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace ServiciosWeb.WS
{
    [ServiceContract]
    public interface IEstudiantePorAsignatura
    {
        [OperationContract]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "GET", UriTemplate = "GetProgramasAcademicos?IdPrograma={IdPrograma}&IdAsignatura={IdAsignatura}", BodyStyle = WebMessageBodyStyle.Bare)]
        List<EstuXMateria> EstudiantesPorAsignaturaYprograma(string IdPrograma, string IdAsignatura);


        [OperationContract]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "GET", UriTemplate = "GetNotasXEstudiante?IdAsignaturaEstu={IdAsignaturaEstu}", BodyStyle = WebMessageBodyStyle.Bare)]
        List<NotasModel> ObtenerNotasPorEstudiante(string IdAsignaturaEstu);


        [OperationContract]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "GET", UriTemplate = "EstudiantesGeneral", BodyStyle = WebMessageBodyStyle.Bare)]
        List<EstuXMateria> EstudiantesGeneral();
    }
}
