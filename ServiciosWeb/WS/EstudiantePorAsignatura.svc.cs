﻿using ServiciosWeb.Models;
using ServiciosWeb.Models.DTO;
using ServiciosWeb.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ServiciosWeb.WS
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "EstudiantePorAsignatura" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select EstudiantePorAsignatura.svc or EstudiantePorAsignatura.svc.cs at the Solution Explorer and start debugging.
    public class EstudiantePorAsignatura : IEstudiantePorAsignatura
    {
        public List<EstuXMateria> EstudiantesPorAsignaturaYprograma(string IdPrograma, string IdAsignatura)
        {
            Context _context = new Context();
            Error _error = new Error();
            try
            {
                int IdProgra = Convert.ToInt32(IdPrograma);
                int IdAsigna = Convert.ToInt32(IdAsignatura);
                var ConsultaEstudiantesporMateria = _context.EstudiantesAsignaturas.Where(s => s.Asignatura.IdPrograma == IdProgra
                                                                                               && s.Asignatura.IdAsignatura == IdAsigna).Select(s => new EstuXMateria
                {
                    IdEstudianteAsignatura = s.IdEstudianteAsignatura,
                    IdAsignatura = s.IdAsignatura,
                    nombreAsig = s.Asignatura.nombre,
                    IdPrograma = s.Estudiante.IdPrograma,
                    nombrePrograma = s.Asignatura.ProgramaAcademico.nombre,
                    cedulaEstudiante = s.cedula,
                    nombresEstudiante = s.Estudiante.nombres,
                    celular = s.Estudiante.celular
                }).ToList();
                return ConsultaEstudiantesporMateria;
            }
            catch (Exception e)
            {
                _error.IdError = 400;
                _error.NombreError = "Bad Request";
                _error.Descripcion = "Ocurrio un error por favor comuniquese con el adminstrador";
                LogErroresSistema.CrearLog(e);
                throw;
            }
        }

        public List<NotasModel> ObtenerNotasPorEstudiante(string IdAsignaturaEstu)
        {
            Context _context = new Context();
            Error _error = new Error();
            try
            {
                int IdAsignaturaEst = Convert.ToInt32(IdAsignaturaEstu);
                var ConsultaNotasPorEstudiane = _context.Notas.Where(s => s.IdAsignaturaEstudiante == IdAsignaturaEst).Select(s => new NotasModel
                                                                                               {
                                                                                                   nota = s.Nota,
                                                                                                   Idnota = s.IdNota,
                                                                                                   corte = s.Corte,
                                                                                                   idasignaturaEstudiante = s.IdAsignaturaEstudiante,
                                                                                                   NombreAsinatura = s.EstudiantesAsignatura.Asignatura.nombre
                                                                                               }).ToList();
                return ConsultaNotasPorEstudiane;
            }
            catch (Exception e)
            {
                _error.IdError = 400;
                _error.NombreError = "Bad Request";
                _error.Descripcion = "Ocurrio un error por favor comuniquese con el adminstrador";
                LogErroresSistema.CrearLog(e);
                throw;
            }
        }

        public List<EstuXMateria> EstudiantesGeneral()
        {
            Context _context = new Context();
            Error _error = new Error();
            try
            {
               
                var ConsultaGeneral= _context.EstudiantesAsignaturas.Select(s => new EstuXMateria
                                                                                               {
                                                                                                   IdEstudianteAsignatura = s.IdEstudianteAsignatura,
                                                                                                   IdAsignatura = s.IdAsignatura,
                                                                                                   nombreAsig = s.Asignatura.nombre,
                                                                                                   IdPrograma = s.Estudiante.IdPrograma,
                                                                                                   nombrePrograma = s.Asignatura.ProgramaAcademico.nombre,
                                                                                                   cedulaEstudiante = s.cedula,
                                                                                                   nombresEstudiante = s.Estudiante.nombres,
                                                                                                   celular = s.Estudiante.celular,
                                                                                                   Docente = s.Asignatura.ProgramaAcademico.Profesors.FirstOrDefault().nombres
                                                                                               }).ToList();
                return ConsultaGeneral;
            }
            catch (Exception e)
            {
                _error.IdError = 400;
                _error.NombreError = "Bad Request";
                _error.Descripcion = "Ocurrio un error por favor comuniquese con el adminstrador";
                LogErroresSistema.CrearLog(e);
                throw;
            }
        }

    }
}
