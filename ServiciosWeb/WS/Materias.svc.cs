﻿using ServiciosWeb.Models;
using ServiciosWeb.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web.Script.Serialization;

namespace ServiciosWeb.WS
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Materias" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Materias.svc or Materias.svc.cs at the Solution Explorer and start debugging.
    public class Materias : IMaterias
    {

        public List<ModelAsignatura> GetAsignaturas()
        {
            Context _context = new Context();
            Error _error = new Error();
            JavaScriptSerializer json = new JavaScriptSerializer();
            try
            {
                var ConsultaAsignaturas = _context.Asignaturas.Select(s => new ModelAsignatura
                {
                    IdAsignatura = s.IdAsignatura,
                    nombre = s.nombre,
                    IdPrograma = s.IdPrograma
                }).ToList();
                return ConsultaAsignaturas;
            }
            catch (Exception e)
            {
                _error.IdError = 400;
                _error.NombreError = "Bad Request";
                _error.Descripcion = "Ocurrio un error por favor comuniquese con el adminstrador";
                LogErroresSistema.CrearLog(e);
                throw;
            }
        }


        public List<ModelAsignatura> GetAsignaturasxIdPrograma(string IdPrograma)
        {
            Context _context = new Context();
            Error _error = new Error();
            JavaScriptSerializer json = new JavaScriptSerializer();
            try
            {
                int ID = Convert.ToInt32(IdPrograma);
                var ConsultaAsignaturas = _context.Asignaturas.Where(a => a.IdPrograma == ID).Select(s => new ModelAsignatura
                {
                    IdAsignatura = s.IdAsignatura,
                    nombre = s.nombre,
                    IdPrograma = s.IdPrograma
                }).ToList();
                return ConsultaAsignaturas;
            }
            catch (Exception e)
            {
                _error.IdError = 400;
                _error.NombreError = "Bad Request";
                _error.Descripcion = "Ocurrio un error por favor comuniquese con el adminstrador";
                LogErroresSistema.CrearLog(e);
                throw;
            }
        }

    }
}
