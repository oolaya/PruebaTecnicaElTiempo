﻿using ServiciosWeb.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace ServiciosWeb.WS
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "INotasServices" in both code and config file together.
    [ServiceContract]
    public interface INotasServices
    {
        [OperationContract]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", RequestFormat = WebMessageFormat.Json, UriTemplate = "PostNotas", BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        bool InsertNotasPorEstudiante(List<NotasModel> model);
    }
}
