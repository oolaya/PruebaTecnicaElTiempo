﻿using ServiciosWeb.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace ServiciosWeb.WS
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IProgramasService" in both code and config file together.
    [ServiceContract]
    public interface IProgramasService
    {
        [OperationContract]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "GET", UriTemplate = "GetProgramasAcademicos", BodyStyle = WebMessageBodyStyle.Bare)]
        List<ProgramasModel> GetProgramasAcademicos();

        [OperationContract]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", RequestFormat = WebMessageFormat.Json, UriTemplate = "InsertPrograma", BodyStyle = WebMessageBodyStyle.Bare)]
        bool PutInsertPrograma(ProgramasModel model);
    }
}
