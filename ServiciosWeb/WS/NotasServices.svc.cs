﻿using ServiciosWeb.Models;
using ServiciosWeb.Models.DTO;
using ServiciosWeb.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ServiciosWeb.WS
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "NotasServices" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select NotasServices.svc or NotasServices.svc.cs at the Solution Explorer and start debugging.
    public class NotasServices : INotasServices
    {
        public bool InsertNotasPorEstudiante(List<NotasModel> model)
        {
            Context _context = new Context();
            Error _error = new Error();
            try
            {
                foreach (var item in model)
                {
                    if (item.corte > 0 && item.corte < 4 && item.editar == 1)
                    {
                        _context.Notas.Add(new Notas
                        {
                            Corte = item.corte,
                            IdAsignaturaEstudiante = item.idasignaturaEstudiante,
                            Nota = item.nota
                        });
                    }
                }
                _context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                _error.IdError = 400;
                _error.NombreError = "Bad Request";
                _error.Descripcion = "Ocurrio un error por favor comuniquese con el adminstrador";
                LogErroresSistema.CrearLog(e);
                return false;
            }

        }
    }
}
