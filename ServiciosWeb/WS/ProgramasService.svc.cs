﻿using ServiciosWeb.Models;
using ServiciosWeb.Models.DTO;
using ServiciosWeb.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ServiciosWeb.WS
{
    public class ProgramasService : IProgramasService
    {
        public List<ProgramasModel> GetProgramasAcademicos()
        {
            Context _context = new Context();
            Error _error = new Error();
            try
            {
                var ConsultaAsignaturas = _context.ProgramaAcademicoes.Select(s => new ProgramasModel
                {
                    IdPrograma = s.IdPrograma,
                    nombre = s.nombre
                }).ToList();
                return ConsultaAsignaturas;
            }
            catch (Exception e)
            {
                _error.IdError = 400;
                _error.NombreError = "Bad Request";
                _error.Descripcion = "Ocurrio un error por favor comuniquese con el adminstrador";
                LogErroresSistema.CrearLog(e);
                throw;
            }
        }

        public bool PutInsertPrograma(ProgramasModel model)
        {
            Context _context = new Context();
            ProgramaAcademico _Progra = new ProgramaAcademico();
            Error _error = new Error();
            try
            {
                var exist = _context.ProgramaAcademicoes.Where(s => s.nombre == model.nombre).FirstOrDefault();

                if (exist != null)
                {
                    _Progra.nombre = model.nombre;
                    _context.ProgramaAcademicoes.Add(_Progra);
                    _context.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                _error.IdError = 400;
                _error.NombreError = "Bad Request";
                _error.Descripcion = "Ocurrio un error por favor comuniquese con el adminstrador";
                LogErroresSistema.CrearLog(e);
                throw;
            }
        }
    }
}
