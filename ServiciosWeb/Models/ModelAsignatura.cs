﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiciosWeb.Models
{
    public class ModelAsignatura
    {
        public int IdAsignatura { get; set; }
        public string nombre { get; set; }
        public int IdPrograma { get; set; }
    }
}