namespace ServiciosWeb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Asignatura
    {
        public Asignatura()
        {
           // EstudiantesAsignaturas = new HashSet<EstudiantesAsignatura>();
            profesorAsignaturas = new HashSet<profesorAsignatura>();
        }

        [Key]
        public int IdAsignatura { get; set; }

        public string nombre { get; set; }

        public int IdPrograma { get; set; }

        public virtual ProgramaAcademico ProgramaAcademico { get; set; }

       // public virtual ICollection<EstudiantesAsignatura> EstudiantesAsignaturas { get; set; }

        public virtual ICollection<profesorAsignatura> profesorAsignaturas { get; set; }
    }
}
