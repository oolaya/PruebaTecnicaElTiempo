namespace ServiciosWeb
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using ServiciosWeb.Models;

    public partial class Context : DbContext
    {
        public Context()
            : base("name=Context")
        {
            Database.SetInitializer<Context>(new CreateDatabaseIfNotExists<Context>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public virtual DbSet<Asignatura> Asignaturas { get; set; }
        public virtual DbSet<Estudiante> Estudiantes { get; set; }
        public virtual DbSet<EstudiantesAsignatura> EstudiantesAsignaturas { get; set; }
        public virtual DbSet<LogErrore> LogErrores { get; set; }
        public virtual DbSet<profesorAsignatura> profesorAsignaturas { get; set; }
        public virtual DbSet<Profesor> Profesors { get; set; }
        public virtual DbSet<ProgramaAcademico> ProgramaAcademicoes { get; set; }
        public virtual DbSet<Notas> Notas { get; set; }
    }
}
