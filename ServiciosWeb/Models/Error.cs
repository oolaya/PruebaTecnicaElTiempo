﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiciosWeb.Models
{
    public class Error
    {
        public int IdError { get; set; }
        public string NombreError { get; set; }
        public string Descripcion { get; set; }
    }
}