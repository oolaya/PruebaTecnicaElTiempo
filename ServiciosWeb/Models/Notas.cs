﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ServiciosWeb.Models
{
    public class Notas
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int IdNota { get; set; }
        public double Nota { get; set; }
        public int Corte { get; set; }
        public int IdAsignaturaEstudiante { get; set; }

        [ForeignKey("IdAsignaturaEstudiante")]
        public virtual EstudiantesAsignatura EstudiantesAsignatura { get; set; }
    }
}