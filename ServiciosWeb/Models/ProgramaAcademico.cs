namespace ServiciosWeb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ProgramaAcademico
    {
        public ProgramaAcademico()
        {
            Asignaturas = new HashSet<Asignatura>();
            Estudiantes = new HashSet<Estudiante>();
            Profesors = new HashSet<Profesor>();
        }

        [Key]
        public int IdPrograma { get; set; }

        public string nombre { get; set; }

        public virtual ICollection<Asignatura> Asignaturas { get; set; }

        public virtual ICollection<Estudiante> Estudiantes { get; set; }

        public virtual ICollection<Profesor> Profesors { get; set; }
    }
}
