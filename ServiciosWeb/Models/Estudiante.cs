namespace ServiciosWeb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Estudiante
    {
        public Estudiante()
        {
            EstudiantesAsignaturas = new HashSet<EstudiantesAsignatura>();
        }

        [Key]
        public int cedula { get; set; }

        public string nombres { get; set; }

        public string celular { get; set; }

        public int IdPrograma { get; set; }

        public virtual ProgramaAcademico ProgramaAcademico { get; set; }

        public virtual ICollection<EstudiantesAsignatura> EstudiantesAsignaturas { get; set; }
    }
}
