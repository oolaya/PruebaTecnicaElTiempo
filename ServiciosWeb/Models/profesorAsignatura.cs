namespace ServiciosWeb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class profesorAsignatura
    {
        [Key]
        public int IdprofesorAsignatura { get; set; }

        public int cedula { get; set; }

        public int IdAsignatura { get; set; }

        public virtual Asignatura Asignatura { get; set; }

        public virtual Profesor Profesor { get; set; }
    }
}
