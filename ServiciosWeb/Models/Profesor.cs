namespace ServiciosWeb
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Profesor
    {
        public Profesor()
        {
            profesorAsignaturas = new HashSet<profesorAsignatura>();
        }

        [Key]
        public int cedula { get; set; }

        public string nombres { get; set; }

        public int IdPrograma { get; set; }

        public virtual ICollection<profesorAsignatura> profesorAsignaturas { get; set; }

        public virtual ProgramaAcademico ProgramaAcademico { get; set; }
    }
}
