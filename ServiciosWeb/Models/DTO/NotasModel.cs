﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiciosWeb.Models.DTO
{
    public class NotasModel
    {
        public int Idnota { get; set; }
        public double nota { get; set; }
        public int corte { get; set; }
        public int idasignaturaEstudiante { get; set; }
        public string NombreAsinatura { get; set; }
        public int editar { get; set; }
    }
}