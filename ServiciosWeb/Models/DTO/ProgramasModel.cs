﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiciosWeb.Models.DTO
{
    public class ProgramasModel
    {
        public int IdPrograma { get; set; }
        public string nombre { get; set; }
    }
}