﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ServiciosWeb.Models.DTO
{
    public class EstuXMateria
    {
        public int IdEstudianteAsignatura { get; set; }
        public int IdAsignatura { get; set; }
        public string nombreAsig { get; set; }
        public int IdPrograma { get; set; }
        public string nombrePrograma { get; set; }
        public int cedulaEstudiante { get; set; }
        public string nombresEstudiante { get; set; }
        public string celular { get; set; }
        public string Docente { get; set; }
    }
}