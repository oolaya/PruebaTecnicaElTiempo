(function () {
    var _templategrid;
    // Private function
    function getColumnsForScaffolding(data) {
        if ((typeof data.length !== 'number') || data.length === 0) {
            return [];
        }
        var columns = [];
        for (var propertyName in data[0]) {
            columns.push({ headerText: propertyName, rowText: propertyName });
        }
        return columns;
    }

    ko.simpleGrid = {
        // Defines a view model class you can use to populate a grid
        viewModel: function (configuration, templategrid) {
            _templategrid = templategrid;
            this.data = configuration.data;
            this.currentPageIndex = ko.observable(0);
            this.pageSize = configuration.pageSize || 5;

            // If you don't specify columns configuration, we'll use scaffolding
            this.columns = configuration.columns || getColumnsForScaffolding(ko.utils.unwrapObservable(this.data));
            this.dataSize = this.columns.length;

            this.itemsOnCurrentPage = ko.computed(function () {
                var startIndex = this.pageSize * this.currentPageIndex();
                return ko.utils.unwrapObservable(this.data).slice(startIndex, startIndex + this.pageSize);
            }, this);

            this.maxPageIndex = ko.computed(function () {
                return Math.ceil(ko.utils.unwrapObservable(this.data).length / this.pageSize) - 1;
            }, this);
        }
    };

    // Templates used to render the grid
    var templateEngine = new ko.nativeTemplateEngine();

    templateEngine.addTemplate = function (templateName, templateMarkup) {
        document.write("<script type='text/html' id='" + templateName + "'>" + templateMarkup + "<" + "/script>");
    };

    templateEngine.addTemplate("ko_simpleGrid_grid", "\
                        <table id=\"texcel\" class=\"table table-striped\" cellspacing=\"0\">\
                        <thead>\
                            <tr data-bind=\"foreach: columns\">\
                               <th data-bind=\"text: headerText\"></th>\
                            </tr>\
                        </thead>\
                        <tbody data-bind=\"foreach: itemsOnCurrentPage\">\
                           <tr data-bind=\"foreach: $parent.columns\">\
                               <td><span data-bind=\"text: typeof rowText == 'function' ? rowText($parent) == null ? '' : rowText($parent) : $parent[rowText] == null ? '' : $parent[rowText] \" ></span></td>\
                               <td data-bind=\"visible: $index() == ($root.dataSize-1) \"> <input class='btn btn-link BtnEditar' type='button' data-bind=\"value : 'editar'\" id='editarBtn' /></td>\
                            </tr>\
                        </tbody>\
                    </table>");
    templateEngine.addTemplate("ko_simpleGrid_Normal", "\
                        <table id=\"texcel\" class=\"table table-striped\" cellspacing=\"0\">\
                        <thead>\
                            <tr id=\"cabecerasTable\" data-bind=\"foreach: columns\">\
                               <th data-bind=\"text: headerText\"></th>\
                            </tr>\
                        </thead>\
                        <tbody data-bind=\"foreach: itemsOnCurrentPage\">\
                           <tr data-bind=\"foreach: $parent.columns\">\
                               <td><span data-bind=\"text: typeof rowText == 'function' ? rowText($parent) == null ? '' : rowText($parent) : $parent[rowText] == null ? '' : $parent[rowText] \" ></span></td>\
                            </tr>\
                        </tbody>\
                    </table>");

    templateEngine.addTemplate("ko_simpleGrid_pageLinks", "\
                    <div class=\"ko-grid-pageLinks\">\
                        <span>Page:</span>\
                        <!-- ko foreach: ko.utils.range(0, maxPageIndex) -->\
                               <a href=\"#\" data-bind=\"text: $data + 1, click: function() { $root.currentPageIndex($data) }, css: { selected: $data == $root.currentPageIndex() }\">\
                            </a>\
                        <!-- /ko -->\
                    </div>");

    templateEngine.addTemplate("ko_simpleGrid_carges", "\
                        <table id=\"texcel\" class=\"ko-grid table\" cellspacing=\"0\">\
                        <thead>\
                            <tr data-bind=\"foreach: columns\">\
                               <th data-bind=\"text: headerText\"></th>\
                            </tr>\
                        </thead>\
                        <tbody data-bind=\"foreach: itemsOnCurrentPage\">\
                           <tr data-bind=\"foreach: $parent.columns\">\
                               <td><input type='text' data-bind=\"value: typeof rowText == 'function' ? rowText($parent) : $parent[rowText] \"/></td>\
                            </tr>\
                        </tbody>\
                    </table>");

    templateEngine.addTemplate("ko_simpleGrid_cargesApro", "\
                        <table id=\"texcel\" class=\"ko-grid table\" cellspacing=\"0\">\
                        <thead>\
                            <tr data-bind=\"foreach: columns\">\
                               <th data-bind=\"text: headerText\"></th>\
                            </tr>\
                        </thead>\
                        <tbody data-bind=\"foreach: itemsOnCurrentPage\">\
                           <tr data-bind=\"foreach: $parent.columns\">\
                               <td><span data-bind=\"text: typeof rowText == 'function' ? rowText($parent) : $parent[rowText] \"></span></td>\
                            </tr>\
                        </tbody>\
                    </table>");

    templateEngine.addTemplate("ko_simpleGrid_AssinaCuadrilla", "\
                        <table id=\"texcel\" class=\"table table-striped\" cellspacing=\"0\">\
                        <thead>\
                            <tr data-bind=\"foreach: columns\">\
                               <th data-bind=\"text: headerText\"></th>\
                            </tr>\
                        </thead>\
                        <tbody data-bind=\"foreach: itemsOnCurrentPage\">\
                           <tr data-bind=\"foreach: $parent.columns\">\
                               <td class='tes'><span id=\"rowTex\" data-bind=\"text: typeof rowText == 'function' ? rowText($parent) == null ? '' : rowText($parent) : $parent[rowText] == null ? '' : $parent[rowText] \" ></span></td>\
                                <td data-bind=\"visible: $index() == ($root.dataSize-1) \"><button type='button' id='botonTarea' class='btn btn-Editar' >Asignar</button></td>\
                            </tr>\
                        </tbody>\
                    </table>");

    templateEngine.addTemplate("ko_simpleGrid_Detalle", "\
                        <table id=\"texcel\" class=\"table table-striped\" cellspacing=\"0\">\
                        <thead>\
                            <tr data-bind=\"foreach: columns\">\
                               <th data-bind=\"text: headerText\"></th>\
                            </tr>\
                        </thead>\
                        <tbody data-bind=\"foreach: itemsOnCurrentPage\">\
                           <tr data-bind=\"foreach: $parent.columns\">\
                               <td class='tes'><span id=\"rowTex\" data-bind=\"text: typeof rowText == 'function' ? rowText($parent) == null ? '' : rowText($parent) : $parent[rowText] == null ? '' : $parent[rowText] \" ></span></td>\
                                <td data-bind=\"visible: $index() == ($root.dataSize-1) \"><button type='button' id='buttonDetails' class='btn btn-Editar' >Detalles</button></td>\
                            </tr>\
                        </tbody>\
                    </table>");

    templateEngine.addTemplate("ko_simpleGrid_gridSubActividades", "\
                        <table id=\"texcel\" class=\"table table-striped\" cellspacing=\"0\">\
                        <thead>\
                            <tr data-bind=\"foreach: columns\">\
                               <th data-bind=\"text: headerText\"></th>\
                            </tr>\
                        </thead>\
                        <tbody data-bind=\"foreach: itemsOnCurrentPage\">\
                           <tr data-bind=\"foreach: $parent.columns\">\
                               <td><span data-bind=\"text: typeof rowText == 'function' ? rowText($parent) == null ? '' : rowText($parent) : $parent[rowText] == null ? '' : $parent[rowText] \" ></span></td>\
                               <td data-bind=\"visible: $index() == ($root.dataSize-1) \"> <input class='btn btn-link BtnEditar' type='button' data-bind=\"value : 'Subir'\" id='subirBtn' />  <input class='btn btn-link BtnEditar' type='button' data-bind=\"value : 'bajar'\" id='bajarBtn' /></td>\
                               <td data-bind=\"visible: $index() == ($root.dataSize-1) \"> <input class='btn btn-link BtnEditar' type='button' data-bind=\"value : 'editar'\" id='editarBtn' /></td>\
                            </tr>\
                        </tbody>\
                    </table>");
    templateEngine.addTemplate("ko_simpleGrid_Solicitudes", "\
                        <table id=\"texcel\" class=\"table table-striped\" cellspacing=\"0\">\
                        <thead>\
                            <tr data-bind=\"foreach: columns\">\
                               <th data-bind=\"text: headerText\"></th>\
                            </tr>\
                        </thead>\
                        <tbody data-bind=\"foreach: itemsOnCurrentPage\">\
                           <tr data-bind=\"foreach: $parent.columns\">\
                               <td class='tes'><small><span id=\"rowTex\" data-bind=\"text: typeof rowText == 'function' ? rowText($parent) == null ? '' : rowText($parent) : $parent[rowText] == null ? '' : $parent[rowText] \" ></span></small></td>\
                                <td data-bind=\"visible: $index() == ($root.dataSize-1) \"><button type='button' id='buttonAtender' class='btn btn-success' data-toggle='tooltip' data-placement='right' title='Atender Solicitud'>Atender</button></td>\
                                <td data-bind=\"visible: $index() == ($root.dataSize-1) \"><button type='button' id='buttonDescargarArchivo' class='btn btn-info active' data-toggle='tooltip' data-placement='right' title='Descargar Archivo'><span class='glyphicon glyphicon-cloud-download'></span></button></td>\
                            </tr>\
                        </tbody>\
                    </table>");
    templateEngine.addTemplate("ko_simpleGrid_Devoluciones", "\
                        <table id=\"texcel1\" class=\"table table-striped\" cellspacing=\"0\">\
                        <thead>\
                            <tr data-bind=\"foreach: columns\">\
                               <th data-bind=\"text: headerText\"></th>\
                            </tr>\
                        </thead>\
                        <tbody data-bind=\"foreach: itemsOnCurrentPage\">\
                           <tr data-bind=\"foreach: $parent.columns\">\
                               <td ><small><input class=\"form-control\" disabled type=\"text\" id=\"rowTex\" data-bind=\"value: typeof rowText == 'function' ? rowText($parent) == null ? '' : rowText($parent) : $parent[rowText] == null ? '' : $parent[rowText] \" ></small></td>\
                                <td class='tes col-sm-1' data-bind=\"visible: $index() == ($root.dataSize-1) \"><input type='text' class='form-control'placeholder='Valor retornado'onkeypress='return SoloNumeros(event);' required/></td>\
                                <td class='tes col-sm-1' data-bind=\"visible: $index() == ($root.dataSize-1) \"><select class=\"selectDevolucion form-control\" ></td>\
                                <td class='tes col-sm-1' data-bind=\"visible: $index() == ($root.dataSize-1) \"><input type='text' class='form-control' placeholder='Observacion' required/></td>\
                                <td class='tes col-sm-1' data-bind=\"visible: $index() == ($root.dataSize-1) \"><select id=\"idubicacion\" class=\"selectUbicacion form-control\" ></td>\
                            </tr>\
                        </tbody>\
                    </table>");
    // The "simpleGrid" binding
    ko.bindingHandlers.simpleGrid = {
        init: function () {
            return { 'controlsDescendantBindings': true };
        },
        // This method is called to initialize the node, and will also be called again if you change what the grid is bound to
        update: function (element, viewModelAccessor, allBindingsAccessor) {
            var viewModel = viewModelAccessor(), allBindings = allBindingsAccessor();

            // Empty the element
            while (element.firstChild)
                ko.removeNode(element.firstChild);

            // Allow the default templates to be overridden
            var gridTemplateName = allBindings.simpleGridTemplate || _templategrid,
                pageLinksTemplateName = allBindings.simpleGridPagerTemplate || "ko_simpleGrid_pageLinks";

            // Render the main grid
            var gridContainer = element.appendChild(document.createElement("DIV"));
            ko.renderTemplate(gridTemplateName, viewModel, { templateEngine: templateEngine }, gridContainer, "replaceNode");

            // Render the page links
            var pageLinksContainer = element.appendChild(document.createElement("DIV"));
            ko.renderTemplate(pageLinksTemplateName, viewModel, { templateEngine: templateEngine }, pageLinksContainer, "replaceNode");

        }
    };
})();