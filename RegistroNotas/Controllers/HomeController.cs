﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RegistroNotas.Controllers
{
    public class HomeController : Controller
    {
        private string ProgramasServices = ConfigurationManager.AppSettings["ProgramasServices"];
        private string MateriaServices = ConfigurationManager.AppSettings["MateriaServices"];
        private string EstudiantePorAsignatura = ConfigurationManager.AppSettings["EstudiantePorAsignatura"];
        private string NotasServices = ConfigurationManager.AppSettings["NotasServices"];

        public ActionResult Index()
        {
            ViewBag.MateriaServices = MateriaServices;
            ViewBag.ProgramasServices = ProgramasServices;
            ViewBag.EstudiantePorAsignatura = EstudiantePorAsignatura;
            ViewBag.NotasServices = NotasServices;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.MateriaServices = MateriaServices;
            ViewBag.ProgramasServices = ProgramasServices;
            ViewBag.EstudiantePorAsignatura = EstudiantePorAsignatura;
            ViewBag.NotasServices = NotasServices;
            return View();
        }

        public ActionResult Contact()
        {
     
            return View();
        }
    }
}