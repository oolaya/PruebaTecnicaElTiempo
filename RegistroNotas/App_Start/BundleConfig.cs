﻿using System.Web;
using System.Web.Optimization;

namespace RegistroNotas
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                      "~/Scripts/jquery-{version}.js",
                       "~/Scripts/jquery-ui.min.js",
                       "~/Scripts/Util.js",
                       "~/Scripts/UniversalHelper.js",
                       "~/Scripts/jquery.colorbox-min.js",
                       "~/Scripts/jquery.prefixfree-1.0.7.js",
                       "~/Scripts/knockout-3.1.0.js",
                       "~/Scripts/bootbox.js",
                       "~/Scripts/jquery.dataTables.js",
                       "~/Scripts/dataTables.scroller.min.js",
                       "~/Scripts/knockout.simpleGrid.1.3.js",
                       "~/Scripts/accounting.js",
                       "~/Scripts/jquery.tabletojson.js",
                       "~/Scripts/moment-with-locales.min.js",
                       "~/Scripts/jSignature.js",
                       "~/Scripts/jSignature.CompressorSVG.js",
                       "~/Scripts/dataTables.buttons.min.js",
                       "~/Scripts/buttons.flash.min.js",
                        "~/Scripts/jszip.min.js",
                        "~/Scripts/pdfmake.min.js",
                        "~/Scripts/vfs_fonts.js",
                        "~/Scripts/d3.v3.min.js",
                        "~/Scripts/c3.min.js",
                        "~/Scripts/jquery-1.12.3.min.js",
                        "~/Scripts/buttons.html5.min.js",
                        "~/Scripts/Chart.bundle.js",
                        "~/Scripts/Chart.bundle.min.js",
                        "~/Scripts/Chart.js",
                        "~/Scripts/Chart.min.js",
                        "~/Scripts/passport.js",
                        "~/Scripts/dataTables.responsive.min.js",
                        "~/Scripts/buttons.print.min.js",
                        "~/Scripts/OpenLayers.js",
                        "~/Scripts/qrcode.min.js",
                        "~/Scripts/sweetalert-dev.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/jquery-ui.css",
                      "~/Content/colorbox.css",
                      "~/Content/jquery.dataTables.min.css",
                      "~/Content/Menu.css",
                      "~/Content/c3.css",
                      "~/Content/jquery-ui-1.8.16.custom.css",
                      "~/Content/buttons.dataTables.min.css",
                      "~/Content/responsive.dataTables.min.css",
                      "~/Content/sweetalert.css"
                      ));
        }
    }
}
