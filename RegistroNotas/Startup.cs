﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RegistroNotas.Startup))]
namespace RegistroNotas
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
