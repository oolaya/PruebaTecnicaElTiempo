﻿$(function () {
    $.ajaxSetup({
        error: function (jqXHR, exception) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                bootbox.alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                bootbox.alert('Internal Server Error [500].');
            } else if (jqXHR.status == 300) {
                bootbox.alert('Ambiguo Error [300]');
            }
            //else if (exception === 'parsererror') {
                //    bootbox.alert('Requested JSON parse failed.');se cometario en el caso al registrar el usuario
            //}
            else if (exception === 'timeout') {
                bootbox.alert('Ambiguo Error [300]. Revise la lista de errores');
            }else if (jqXHR.status == 409) {
                bootbox.alert('Conflicto. El dato no existe en la base de datos. Revise la lista de errores');
            }else if (exception === 'parsererror') {
                bootbox.alert('Requested JSON parse failed.');
            } else if (exception === 'timeout') {
                bootbox.alert('Time out error.');
            } else if (exception === 'abort') {
                bootbox.alert('Ajax request aborted.');
            }
            //else {
            //    bootbox.alert('Uncaught Error: ' + jqXHR.responseText); se cometario en el caso al registrar el usuario
            //}
        }
    });
});

transport = function () {

    var verb = function () {
        var post = "POST";
        var get = "GET";
        var put = "PUT";
        var del = "DELETE";

        return {
            post: post,
            get: get,
            put: put,
            del: del
        };
    }();

    var executeRequest = function (
        method,
        type,
        url,
        data,
        doneCallback,
        failCallback) {

        var isCrossDomainRequest = url.indexOf('http://') == 0
            || url.indexOf('https://') == 0;

        if (window.XDomainRequest
            && $.browser.msie
            && $.browser.version < 10
            && isCrossDomainRequest) {

            // IE 10 fully supports XMLHttpRequest 
            //  but still contains XDomainRequest. 
            // Include check for IE < 10 to force IE 10 calls 
            //  to use standard XMLHttpRequest instead.
            // Only use XDomainRequest for cross-domain calls.

            var xdr = new XDomainRequest();
            if (xdr) {
                xdr.open(method, url);
                xdr.onload = function () {
                    var result = $.parseJSON(xdr.responseText);
                    if (result === null
                        || typeof (result) === 'undefined') {
                        result = $.parseJSON(
                            data.firstChild.textContent);
                    }

                    if ($.isFunction(doneCallback)) {
                        doneCallback(result);
                    }
                };
                xdr.onerror = function () {
                    if ($.isFunction(failCallback)) {
                        failCallback();
                    }
                };
                xdr.onprogress = function () { };
                xdr.send(data);
            }
            return xdr;
        } else {
            var request = $.ajax({
                type: method,
                url: url,
                data: data,
                dataType: type,
                cache: false,
                contentType: 'application/json; charset=utf-8',
                beforeSend: function () { UniversalHelper.loaderAnimate() },
                complete: function () { UniversalHelper.loaderAnimateClose() }
            }).done(function (result) {
                if ($.isFunction(doneCallback)) {
                    doneCallback(result);
                }
            }).fail(function (jqXhr, textStatus) {
                if ($.isFunction(failCallback)) {
                    failCallback(jqXhr, textStatus);
                }
            }).error(function (jqXHR) { });
            return request;
        }
    };

    return {
        verb: verb,
        executeRequest: executeRequest
    };
}();