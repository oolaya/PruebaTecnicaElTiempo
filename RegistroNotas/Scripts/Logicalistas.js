﻿$().ready(function () {
    $('.PasarEquipo').click(function () {
        return !$('#OrigenEquipo option:selected').remove().appendTo('#DestinoEquipo');
    });

    $('.QuitarEquipo').click(function () {
        return !$('#DestinoEquipo option:selected').remove().appendTo('#OrigenEquipo');
    });

    $('.PasarEquipos').click(function () {
        $('#OrigenEquipo option').each(function () { $(this).remove().appendTo('#DestinoEquipo'); });
    });

    $('.QuitarEquipos').click(function () {
        $('#DestinoEquipo option').each(function () { $(this).remove().appendTo('#OrigenEquipo'); });
    });


    $('.PasarMaterial').click(function () {
        return !$('#OrigenMaterial option:selected').remove().appendTo('#DestinoMaterial');
    });

    $('.QuitarMaterial').click(function () {
        return !$('#DestinoMaterial option:selected').remove().appendTo('#OrigenMaterial');
    });

    $('.PasarMateriales').click(function () {
        $('#OrigenMaterial option').each(function () { $(this).remove().appendTo('#DestinoMaterial'); });
    });

    $('.QuitarMateriales').click(function () {
        $('#DestinoMaterial option').each(function () { $(this).remove().appendTo('#OrigenMaterial'); });
    });
});

