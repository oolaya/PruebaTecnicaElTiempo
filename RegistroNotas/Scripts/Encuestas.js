
var Encuestas = (function () {

    //=======================================Inicio:  Variables globales==============================//
    var listModelSave = new Array();
    var listModelListSaveChange = new Array();
    var endPoll = -1;
    var DateIni = null;
    var IdEncuestaGlobal = 0;
    var IdCampo = 0;
    var TableEnlace = '';
    var estadoSavelist = false;
    //=======================================Fin:   Variables globales===============================//

    /*
    Modelo para guardar los datos en el local storage.
    */
    function ModelListSave(id, type) {
        this.Id = id;
        this.Type = type;
    };

    /*
    Modelo para armar la lista del modelo del api.
    */
    function ModelListSaveChange(idPregunta, idOpcionRespuesta, textoNumber, idUserEncuesta, IdReporteEnlaceEncuesta) {
        this.idPregunta = idPregunta;
        this.idOpcionRespuesta = idOpcionRespuesta;
        this.textoNumber = textoNumber;
        this.IdUsuario = idUserEncuesta;
        this.IdReporteEnlaceEncuesta;
    };

    /*
    Enumerador para la libreria js Encuestas.
    */
    var generalsEnum = function () {
        var divAcordion = "<div class=\"panel-group\" id=\"accordion\"></div>";
        var divPanelDefault = "<div class=\"panel panel-default\"> ";
        var arryaIdControls = [];
        var twoDivClose = "</div></div> ";
        var oneDivClose = "</div> ";
        var divPanelHead = "<div class=\"panel-heading\"> ";
        var h4Title = "<h4 class=\"panel-title\"> ";
        var closeh4 = "</h4> ";
        var aCollapse = "<a data-toggle=\"collapse\"  href=\"";
        var aCloseCollapse = "</a>";
        var classCollapse = "\" class=\"panel-collapse collapse\">";
        var classCollapsein = "\" class=\"panel-collapse collapse in\">";
        var divPanelBody = "<div class=\"panel-body\" ";

        return {
            divAcordion: divAcordion,
            divPanelDefault: divPanelDefault,
            arryaIdControls: arryaIdControls,
            twoDivClose: twoDivClose,
            oneDivClose: oneDivClose,
            divPanelHead: divPanelHead,
            h4Title: h4Title,
            closeh4: closeh4,
            classCollapse: classCollapse,
            divPanelBody: divPanelBody,
            aCollapse: aCollapse,
            aCloseCollapse: aCloseCollapse,
            classCollapsein: classCollapsein
        };

    }();

    /*
        create  :   apaez
        date    :   26092016
        reason  :   buscar el permiso para la encuesta
    */
    var verificarPermisoEncuesta = function (callback, nombre) {
        transport.executeRequest(
           transport.verb.get,
           'jsonp',
	        urlApiE + '/api/PermisoXEncuestaLoad?filtro=getPermisoPorEncuesta&nombre=' + nombre,
           null,
           function (data) {
               if (data > 0) {
                   callback(data);
               } else {
                   bootbox.alert("La encuesta aun no tiene permisos por favor solicitar dicho permiso.");
               }
           }, function (jqXhr, textStatus) {
               if (textStatus == 'error') {
                   bootbox.alert('Ocurrio un error al verificar el permiso de la encuesta.!');
               }
           }
        );
    };

    /*
        create  :   apaez
        date    :   26092016
        reason  :   buscar que la encuesta no halla sido contestada mas de una vez
    */
    validarEncuestaXReporteEnlace = function (callback, idCampo, idEncuesta) {
        transport.executeRequest(
                transport.verb.get,
                'jsonp',
                urlApiE + '/api/EncuestaXReporteEnlace?filtro=verificarEncuestaContestada&idCampo=' + idCampo + '&idEncuesta=' + idEncuesta,
                null,
                function (data) {        
                    callback(data);
                }, function (jqXhr, textStatus) {
                    if (textStatus == 'error') {
                        bootbox.alert('Ocurrio un error al verificar el permiso de la encuesta.!');
                    }
                }
            );
    };

    /*
    Inicia la encuesta por un id(Encuesta la cual ya debe haberse creado con anterioridad)
    */
    this.Load = function (nameEncuesta) {
        window.localStorage.removeItem("ListModelEncuesta");
        listModelSave = [];
        listModelListSaveChange = [];
        $('#divMaster').empty();
        IdCampo = idCampoEncuesta;
        TableEnlace = TableEnlaceEncuesta;
        if (!previewPoll) {
            loadData($idEncuestaPreview);
        } else {
            verificarPermisoEncuesta(function (idEncuestaPer) {
                validarEncuestaXReporteEnlace(function (callbak) {
                    if (callbak == 0) {
                        IdEncuestaGlobal = idEncuestaPer;
                        loadData(idEncuestaPer);
                    } else {
                        bootbox.alert("La encuesta ya fue constestada por favor continue con el proceso");
                        $('#divMaster').empty();
                        $('#divMasterTwo').show();
                    }
                }, IdCampo, idEncuestaPer);
            }, nameEncuesta);
        }
    };


    /*
        create  :   apaez
        date    :   26/09/2016
        reason  :   carga encuestas hijas  por tal motivo este metodo no debe ser usado para para cargar encuestas debido a que no hace la validadacion de si fue contestada o tiene permisos para ser constestada porque
                    la encuesta con el permiso debe ser la encuesta padre,  Tener encuesta que las encuestas hijas son un arbol virtual creado a partir de las encuestas redirect.
    */
    this.LoadEncuestaXidHijas = function(idEncuesta){
        window.localStorage.removeItem("ListModelEncuesta");
        listModelSave = [];
        listModelListSaveChange = [];
        $('#divMaster').empty();
        loadData(idEncuesta);
        IdEncuestaGlobal = idEncuesta;
        IdCampo = idCampoEncuesta;
        TableEnlace = TableEnlaceEncuesta;
    };

    /*
    Adiciona HTML al div maestro el cual se declara el la pagina donde se mostrara la encuesta.
    */
    function addDivMaster(divCreate) {
        $('#divMaster').append(divCreate);
    };

    /*
    Adiciona HTML al div del acordion.
    */
    function addDivAcordion(divCreate) {
        $('#accordion').append(divCreate);
    };

    /*
    Agregar el boton de guardar la encuesta al div maestro
    */
    function addButonSave() {
        if (previewPoll) {
        $('#divMaster').append("<div id=\"idButtonSaveEncuesta\" class=\"form-group\"> <button type=\"button\" class=\"btn btn-primary center-block\" id=\"btnSaveEncuesta\" onclick=\"Encuestas.saveEncuesta(" + endPoll + ");\" >Guardar Encuesta</button></div>");
        }
    };

    /*
    Busca la informacion a para crear y pintar la encuesta desde la base de datos.
    */
    function loadData(idEncuesta) {
        transport.executeRequest(
           transport.verb.get,
           'jsonp',
	        urlApiE + '/api/EncuestaN1?filtro=findEncuentaPreguntaXSegId&idsegmento=' + idSegmentoE + '&idEncuesta=' + idEncuesta,
           null,
           function (data) {
               if (data != null && data.length > 0) {
                   loadPollRedirec(data[0].IdPregunta);
                   addDivMaster(generalsEnum.divAcordion);
                   initPrintHtml(data);
                   addDivAcordion(generalsEnum.oneDivClose);
                   loadDateTime();
               }
           }, function (jqXhr, textStatus) {
               if (textStatus == 'error') {
                   bootbox.alert('Ocurrio un error al cargar la encuesta.!');
               }
           }
        );
    };

    /*
        Carga la fecha y hora donde se inicio la encuesta a ser constestada
    */
    function loadDateTime() {
        DateIni = GetFecha();
    };

    /*
        funcion para cargar el idEncuestaRedirec de la encuesta para determinar el fin de la encuesta o guardar y continuar con
        la otra encuesta.
    */
    function loadPollRedirec(idPregunta) {
        transport.executeRequest(
           transport.verb.get,
           'jsonp',
            urlApiE + '/api/EncuestaN1?filtro=findPollXPregunta&idPregunta=' + idPregunta,
           null,
           function (data) {
               if (data != null && data.length > 0) {
                   endPoll = data[0].IdEncuestaRedirec;
               }
           }, function (jqXhr, textStatus) {
               if (textStatus == 'error') {
                   bootbox.alert('Ocurrio un error al cargar la encuesta Redirec.!');
               }
           }
        );
    };

    /*
    Inicia a crear el html de las preguntas en div acordion.
    */
    function initPrintHtml(data) {

        $.each(data, function (j, item) {

            var divHtml = "";
            var idPnlBody = "pb" + item.IdPregunta;

            divHtml = divHtml + (generalsEnum.divPanelDefault + generalsEnum.divPanelHead + generalsEnum.h4Title);
            divHtml = divHtml + (generalsEnum.aCollapse + "#collapse" + item.IdPregunta + "\"" + "data-parent=\"#accordion" + item.IdPregunta + "\">" + item.Nombre + generalsEnum.aCloseCollapse + generalsEnum.closeh4 + generalsEnum.oneDivClose);

            if (j == 0) {
                divHtml = divHtml + ("<div id=\"" + "collapse" + item.IdPregunta + generalsEnum.classCollapsein);
            } else {
                divHtml = divHtml + ("<div id=\"" + "collapse" + item.IdPregunta + generalsEnum.classCollapse);
            }
            divHtml = divHtml + (generalsEnum.divPanelBody + "id=\'" + idPnlBody + "'" + ">" + generalsEnum.oneDivClose + generalsEnum.twoDivClose);

            addDivAcordion(divHtml);

            if (item.ListPreHijas != null) {
                if (item.ListPreHijas.length > 0) {
                    recursivePrint(item.ListPreHijas, item.IdPregunta, item.IdPregunta);
                }
            }

            idPnlBody = "";
        });
        addButonSave();
        window.localStorage.setItem("ListModelEncuesta", JSON.stringify(listModelSave));
    };

    /*
    Funcion recursiva encargada de crear las hijas de las preguntas padre en forma de arbol.
    */
    function recursivePrint(listEncuesta, idPadre, idHija) {

        $.each(listEncuesta, function (i, item) {

            var divHtml = "";
            var idPnlBody = "";

            if (item.ObjControlWeb != null) {
                var option = item.ObjControlWeb.TipoElementoHtml5;
                if (option == null) {
                    option = item.ObjControlWeb.ElementoHtml5;
                }

                switch (option) {
                    case "checkbox":
                        divHtml = divHtml + createCheckbox(item);
                        break;
                    case "number":
                        divHtml = divHtml + createNumber(item);
                        break;
                    case "radio":
                        divHtml = divHtml + createRadio(item);
                        break;
                    case "select":
                        divHtml = divHtml + createSelect(item);
                        break;
                    case "text":
                        divHtml = divHtml + createText(item);
                        break;
                    case "textarea":
                        divHtml = divHtml + createTextarea(item);
                        break;
                }

            } else {
                var auxIdPanlBody = "";
                auxIdPanlBody = "pb" + idPadre;
                idPnlBody = "pb" + item.IdPregunta;
                addDivXid(auxIdPanlBody, addNewAcordionPanelGoup(item, idPnlBody));
            }

            if (divHtml != "") {
                if (idPnlBody == "") {
                    idPnlBody = "pb" + item.IdPreguntaPadre;
                }
                addDivXid(idPnlBody, divHtml);
            }

            if (item.ListPreHijas != null) {
                if (item.ListPreHijas.length > 0) {
                    recursivePrint(item.ListPreHijas, idPadre, item.IdPreguntaPadre);
                }
            }

        });

    };

    /*
    Crea el html para agrupar los acordion panel en forma de padres hijos etc.
    */
    function addNewAcordionPanelGoup(item, idPnlBody) {
        var divHtml = "";
        divHtml += (generalsEnum.divPanelDefault + generalsEnum.divPanelHead + generalsEnum.h4Title);
        divHtml += (generalsEnum.aCollapse + "#collapse" + item.IdPregunta + "\"" + "data-parent=\"#accordion" + item.IdPregunta + "\">" + item.Nombre + generalsEnum.aCloseCollapse + generalsEnum.closeh4 + generalsEnum.oneDivClose);
        divHtml += ("<div id=\"" + "collapse" + item.IdPregunta + generalsEnum.classCollapse);
        divHtml = divHtml + (generalsEnum.divPanelBody + "id=\'" + idPnlBody + "'" + ">" + generalsEnum.oneDivClose + generalsEnum.twoDivClose);
        divHtml = divHtml + generalsEnum.oneDivClose + generalsEnum.twoDivClose;
        return divHtml;
    };

    /*
    Agrega el html a un acordion panel padre
    */
    function addDivXid(divPnlBody, html) {
        $('#' + divPnlBody).append(html);
    };

    /*
    Crear el control web checkbox de forma dinamica.
    */
    function createCheckbox(item) {
        var html = "";
        html = "<div class=\"form-group\">  <div class=\"col-md-7\"><p>" + item.Nombre + "</p></div>";
        html = html + "<div class=\"col-md-5\">";
        $.each(item.listOpc, function (i, itemOpc) {
            var id = "";
            id = item.IdPregunta + '_' + item.ObjControlWeb.IdControlWeb + '_' + itemOpc.IdOpcionRTa;
            listModelSave.push(new ModelListSave(id, 'checkbox'));
            html = html + "<label class=\"checkbox-inline\"><input name='" + id +
                          "' type='" + item.ObjControlWeb.TipoElementoHtml5 +
                          "' id='" + id + "'> " + itemOpc.Nombre + " </label>";
        });
        html = html + generalsEnum.oneDivClose;

        return html;
    };

    /*
    Crea el control web input de type numerico.
    */
    function createNumber(item) {

        var html = "";
        listModelSave.push(new ModelListSave((item.IdPregunta + ''), 'number'));
        html = "<div class=\"form-group\">  <div class=\"col-md-7\"><p>" + item.Nombre + "</p></div>";
        html = html + "<div class=\"col-md-5\">" +
                     "<input id=" + item.IdPregunta + " name=" + item.IdPregunta + " placeholder=\"" + item.ObjControlWeb.Placeholder.toString() + "\" class=\"form-control input-md\" required type=\"number\" >";
        html = html + generalsEnum.oneDivClose;

        return html;

    };

    /*
    Crear el control web radio de forma dinamica.
    */
    function createRadio(item) {
        var html = "";

        html = "<div class=\"form-group\">  <div class=\"col-md-7\"><p>" + item.Nombre + "</p></div>";
        html = html + "<div class=\"col-md-5\">";
        $.each(item.listOpc, function (i, itemOpc) {
            var id = "";
            id = item.IdPregunta + '_' + item.ObjControlWeb.IdControlWeb + '_' + itemOpc.IdOpcionRTa;
            listModelSave.push(new ModelListSave(id, 'radio'));
            if (itemOpc.IdEncuestareload == null) {
                html = html + "<label class=\"radio-inline\"><input name='" + item.IdPregunta + '_' + item.ObjControlWeb.IdControlWeb +
                                          "' type='" + item.ObjControlWeb.TipoElementoHtml5 +
                                          "' id='" + id + "'> " + itemOpc.Nombre + " </label>";
            } else {
                html = html + "<label class=\"radio-inline\"><input name='" + item.IdPregunta + '_' + item.ObjControlWeb.IdControlWeb +
                                                          "' type='" + item.ObjControlWeb.TipoElementoHtml5 +
                                                          "'onclick=Encuestas.LoadEncuestaXidHijas(" + itemOpc.IdEncuestareload + ") " +
                                                          "  id='" + id + "'> " + itemOpc.Nombre + " </label>";
            }
        });

        html = html + generalsEnum.oneDivClose;

        return html;
    };

    /*
    Crear el control web  select con sus respectivas opciones.
    */
    function createSelect(item) {
        var html = "";
        listModelSave.push(new ModelListSave((item.IdPregunta + ''), 'select'));
        html = "<div class=\"form-group\">  <div class=\"col-md-7\"><p>" + item.Nombre + "</p></div>";
        html = html + "<div class=\"col-md-5\">" + "<select class=\"form-control\" id=" + item.IdPregunta + " name=" + item.IdPregunta + ">";
        html = html + '<option selected value="0" selected>Seleccione >></option>';
        $.each(item.listOpc, function (i, itemOpc) {
            html = html + ('<option  value="' + itemOpc.IdOpcionRTa + '">' + itemOpc.Nombre + '</option>');
        });

        html = html + "</select>" + generalsEnum.oneDivClose;

        return html;
    };

    /*
    Crear el control web de tipo caja de texto
    */
    function createText(item) {

        var html = "";
        listModelSave.push(new ModelListSave((item.IdPregunta + ''), 'text'));
        html = "<div class=\"form-group\">  <div class=\"col-md-7\"><p>" + item.Nombre + "</p></div>";
        html = html + "<div class=\"col-md-5\">" +
                     "<input id=" + item.IdPregunta + " name=" + item.IdPregunta + " placeholder=\"" + item.ObjControlWeb.Placeholder.toString() + "\" class=\"form-control input-md\" required type=\"text\" >";
        html = html + generalsEnum.oneDivClose;

        return html;

    };

    /*
    Crea el control web de textarea.
    */
    function createTextarea(item) {

        var html = "";
        listModelSave.push(new ModelListSave((item.IdPregunta + ''), 'textarea'));
        html = "<div class=\"form-group\">  <div class=\"col-md-7\"><p>" + item.Nombre + "</p></div>";
        html = html + "<div class=\"col-md-5\">" +
                      "<textarea id=" + item.IdPregunta + " name=" + item.IdPregunta + " placeholder=\"" + item.ObjControlWeb.Placeholder.toString() + "\"  class=\"form-control input-md\" rows=\"2\"  style=\"width: 100%;\"></textarea>";
        html = html + generalsEnum.oneDivClose;

        return html;
    };

    /*
    Inicia la validacion dinamica para verificar que todos los campos de la encuesta se completaran de manera correcta y crea la lista para guardar.
    */
    this.saveEncuesta = function () {
        var listLocal = JSON.parse(window.localStorage.getItem("ListModelEncuesta"));
        var estadoBool = true;
        for (var i = 0; i < listLocal.length && estadoBool; i++) {
            switch (listLocal[i].Type) {
                case "checkbox":
                    var padre = listLocal[i].Id.split('_')[0];
                    var k = 0;
                    var boolEntroFor = false;
                    var check = false;
                    if ($('#' + listLocal[i].Id).is(':checked')) {
                        estadoBool = true;
                        check = true;
                        listModelListSaveChange.push(new ModelListSaveChange(listLocal[i].Id.split('_')[0], listLocal[i].Id.split('_')[2], null, idUserEncuesta, 0));
                    }
                    for (var j = i + 1; j < listLocal.length; j++) {
                        boolEntroFor = true;
                        k++;
                        if (listLocal[j].Type != "checkbox") {
                            break;
                        }
                        if (listLocal[j].Id.split('_').length > 1) {
                            if (padre === listLocal[j].Id.split('_')[0]) {
                                if ($('#' + listLocal[j].Id).is(':checked')) {
                                    estadoBool = true;
                                    check = true;
                                    listModelListSaveChange.push(new ModelListSaveChange(listLocal[j].Id.split('_')[0], listLocal[j].Id.split('_')[2], null, idUserEncuesta, 0));
                                }
                            } else if (!check) {
                                estadoBool = false; listModelListSaveChange = []; break;
                            } else if (check) {
                                break;
                            }
                        } else {
                            k++;
                            if (!check) {
                                estadoBool = false; listModelListSaveChange = []; break;
                            }
                            break;
                        }
                    }
                    if (boolEntroFor) {
                        i += (k - 1);
                    }
                    break;
                case "radio":
                    var padre = listLocal[i].Id.split('_')[0];
                    var k = 0;
                    var boolEntroFor = false;
                    var check = false;
                    if ($('#' + listLocal[i].Id).is(':checked')) {
                        estadoBool = true;
                        check = true;
                        listModelListSaveChange.push(new ModelListSaveChange(listLocal[i].Id.split('_')[0], listLocal[i].Id.split('_')[2], null, idUserEncuesta, 0));
                    }
                    for (var j = i + 1; j < listLocal.length; j++) {
                        k++;
                        boolEntroFor = true;
                        if (listLocal[j].Id.split('_').length > 1) {
                            if (padre === listLocal[j].Id.split('_')[0]) {
                                if ($('#' + listLocal[j].Id).is(':checked')) {
                                    estadoBool = true;
                                    check = true;
                                    listModelListSaveChange.push(new ModelListSaveChange(listLocal[j].Id.split('_')[0], listLocal[j].Id.split('_')[2], null, idUserEncuesta, 0));
                                }
                            } else if (!check) {
                                estadoBool = false; listModelListSaveChange = []; break;
                            } else if (check) {
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                    if (boolEntroFor) {
                        i += (k - 1);
                    }
                    break;
                case "number":
                    if (!$('#' + listLocal[i].Id).val().trim() == "") {
                        estadoBool = true;
                        listModelListSaveChange.push(new ModelListSaveChange(listLocal[i].Id, null, $('#' + listLocal[i].Id).val().trim(), idUserEncuesta, 0));
                    } else { estadoBool = false; listModelListSaveChange = []; break; }
                    break;
                case "select":
                    if ($('#' + listLocal[i].Id).val().trim() != "0") {
                        estadoBool = true;
                        listModelListSaveChange.push(new ModelListSaveChange(listLocal[i].Id, $('#' + listLocal[i].Id).val().trim(), null, idUserEncuesta, 0));
                    } else { estadoBool = false; listModelListSaveChange = []; break; }
                    break;
                case "text":
                    if (!$('#' + listLocal[i].Id).val().trim() == "") {
                        estadoBool = true;
                        listModelListSaveChange.push(new ModelListSaveChange(listLocal[i].Id, null, $('#' + listLocal[i].Id).val(), idUserEncuesta, 0));
                    } else { estadoBool = false; listModelListSaveChange = []; break; }
                    break;
                case "textarea":
                    if (!$('#' + listLocal[i].Id).val().trim() == "") {
                        estadoBool = true;
                        listModelListSaveChange.push(new ModelListSaveChange(listLocal[i].Id, null, $('#' + listLocal[i].Id).val(), idUserEncuesta, 0));
                    } else { estadoBool = false; listModelListSaveChange = []; break; }
                    break;
            }
        }
        if (!estadoBool) {
            bootbox.alert('Debe seleccionar todas las repuestas.!');
        } else {
            saveEnlaceEncuesta(listModelListSaveChange);
        }
    };

    /*
        Guarda la lista de opciones de respuesta.
    */
    function save(listJsonRespuestas) {
        transport.executeRequest(
           transport.verb.post,
           'json',
	        urlApiE + '/api/RespuestaXPreguta',
           listJsonRespuestas,
           function (result) {
               if (result == 200) {
                   bootbox.alert('Encuesta guardada satisfactoriamente.!');     
                   if (endPoll != -1) {
                       Encuestas.LoadEncuestaXidHijas(endPoll);
                   } else {
                       $('#divMaster').empty();
                       $('#divMasterTwo').show();
                       $('#divPanelBody').hide();
                   }
               } else {
                   bootbox.alert('Ocurrio un error al guardar la encuesta Pregunta x Respuesta.!');
               }
           }, function (jqXhr, textStatus) {
               if (textStatus == 'error') {
                   bootbox.alert('Ocurrio un error al guardar la encuesta.!');
               }
           }
        );
    };

    /*
       Crear el enlace para guardar el historial de donde se respondio la encuesta. 
    */
    function saveEnlaceEncuesta(listModelEncuestaAdd) {
        transport.executeRequest(
            transport.verb.post,
            'json',
            urlApiE + '/api/EncuestaXReporteEnlace',
            crearJsonSaveEnlaceEncuesta(),
            function (data) {
                if (data != null && data != 400) {
                    var listFinal = addIdEnlaceRespuesta(listModelEncuestaAdd, data.IdEncuestaXReporteEnlace);
                    if (estadoSavelist) {
                        save(JSON.stringify(listFinal));
                    }   
                }
            }, function (jqXhr, textStatus) {
                if (textStatus == 'error') {
                    bootbox.alert('Ocurrio un error al guardar la encuesta enlace!');
                }
            }
        );
    };

    function addIdEnlaceRespuesta(listModelEncuestaAdd, idEnlaceEncuesta) {
        for (var id = 0; id < listModelEncuestaAdd.length ; id++) {
            listModelEncuestaAdd[id].IdReporteEnlaceEncuesta = idEnlaceEncuesta;
            if (id == listModelEncuestaAdd.length - 1) {
                estadoSavelist = true;
            }
        }
        return listModelEncuestaAdd;
    };

    function crearJsonSaveEnlaceEncuesta() {
        return JSON.stringify({
            IdEncuesta: IdEncuestaGlobal,
            FechaInicialString: DateIni,
            IdCampo: IdCampo,
            NombreTable: TableEnlace
        });
    }
    
    return (this);
}).call({});