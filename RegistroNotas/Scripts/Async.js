﻿var Async = (function () {

    this.init = function () {
        // Adding the script tag to the head as suggested before
        var head = document.getElementsByTagName('head')[0];
        var script = document.createElement('script');
        script.src = '//cdn.rawgit.com/jpillora/xdomain/gh-pages/dist/0.5/xdomain.min.js';
        script.setAttribute('slave', 'http://universalapi.colvatel.com/proxy.html');


        // Then bind the event to the callback function.
        // There are several events for cross browser compatibility.
        script.onreadystatechange = callback;
        script.onload = callback;

        // Fire the loading
        head.appendChild(script);
    };

    var callback = function () {
        console.log('script imported successfully.');
    };

    return (this);
}).call({});
