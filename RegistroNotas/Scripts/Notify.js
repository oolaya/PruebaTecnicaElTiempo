﻿var FechaActual = new Date();
var TotalNotify = 0;
var mes = FechaActual.getMonth() + 1;
var FechaF = String(FechaActual.getDate() + '/' + mes + '/' + FechaActual.getFullYear());
var SeeMore = null;
var NoNotify = null;
var classLiAlert = 0;
var UrlApi = null;
var CheckAll = null;
var IdUsuario = null;
var $audio = $("#audioAlarma");
var segmentoN = null;


function NotifyFuntion(ObjNotify) {
    var sp = document.getElementById("STotalNot");
    var sp1 = document.getElementById("STotalNot1");
    $("#MasterUlNotify").empty();
    TotalNotify = 0;
    var RowsLista = [];
    var ObjIndividual1 = [];
    if (ObjNotify.length > 0) {

        $.each(ObjNotify, function (m, value) {
            var hora = value.FechaAlarma.substring(11, value.FechaAlarma.length - 6) + ' h';
            RowsLista.push(
                            '<li>' +
                             '<a href="' + value.UrlTipoAlarma + '" onclick="MarcarLeidoXAlarma(' + value.IdUsuarioAlarma + ')">' +
                                  '<div>' +
                                       '<i class="' + value.LogoTipoAlarma + '"></i><strong> ' + value.DescripcionTipoAlarma + '</strong>' +
                                       '<span class="pull-right text-muted">' +
                                           '<em>' + hora + '</em>' +
                                       '</span>' +
                                   '</div>' +
                                   '<div>' + value.MensajeAlarma.substring(0, 50) + '...</div>' +
                              '</a>' +
                              '<div class="pull-right">' +
                                           '<a href="/Menus/Notificaciones" onclick="MarcarLeidoXAlarma(' + value.IdUsuarioAlarma + ')" style="text-decoration: none; position: absolute; padding: 0 1px 0 9px; margin: -20px 17px 0 -105px;">Leer mas...</a>' +
                                       '</div>' +
                            '</li>' +
                            '<li class="divider"></li>'

                   );
        });

        $("#ImgNotify").attr("src", "../../images/notifysi1.jpg");
        document.getElementById("STotalNot").innerHTML = ObjNotify.length;
        document.getElementById("STotalNot").style.display = "";
        document.getElementById("STotalNot1").style.display = "";
        contador++;
        if (contador == 1000 || contador == 1) {
            contador = 0;
            new Audio('../../Audio/Notify.mp3').play();
        }
        msnPnotify(ObjNotify.length);
    } else {
        $("#ImgNotify").attr("src", "../../images/notifyno.jpg");
        classLiAlert = 'alert-info';
    }
    llenarRows(RowsLista);
}

function llenarRows(li) {
    var RowsListaAux = [];
    if (li.length > 0) {
        for (i = 0; i <= 6; i++) {
            RowsListaAux.push(li[i]);
        }
        TotalNotify = li.length;
        classLiAlert = 'alert-danger';
        NotifyFijas(TotalNotify);
        RowsListaAux.push(SeeMore);
        RowsListaAux.push(CheckAll);

        contador++;
        if (contador == 800) {
            contador = 0;
            new Audio('../../Audio/Notify.mp3').play();
        }


        $("#MasterUlNotify").append(RowsListaAux);

    } else {
        NotifyFijas();
        if (li.length == 0) {
            li.push(NoNotify);
            li.push(SeeMore);
            TotalNotify;
            classLiAlert;
            Change();
        } else {
            TotalNotify = li.length
            classLiAlert = 'alert-danger'
        }

        $("#MasterUlNotify").append(li);
    }
}

function NotifyFijas() {
    SeeMore = '<li class="' + classLiAlert + '">' +
                '<a class="text-center" href="/Menus/Notificaciones">' +
                   '<strong>Ver todas las alertas... </strong>' +
                   '<span class="pull-right text-muted small"> Total: <strong style="font-weight: bold;  color: #000000; font-size: small;"> ' + TotalNotify + '</strong></span>'
    '<i class="fa fa-angle-right"></i>' +
'</a>' +
'</li>';

    CheckAll = '<li class="">' +
        '<div style="text-align: center;" class="checkbox">' +
         '<label>' +
          ' <input id="CheckAll" type="checkbox" value="' + IdUsuario + '" onClick="confirmarCheckAll();">' +
            'Marcar todas como leidas' +
          '</label>' +
        '</div>' +
      '</li>';

    NoNotify = '<li>' +
                   '<a href="#">' +
                       '<div>' +
                           '<i class="fa fa-comments-o"></i> Usted no tiene notificaciones..<span class="pull-right text-muted small"> ' + FechaF + '</span>' +
                        '</div>' +
                   '</a>' +
               '</li>' +
               '<li class="divider"></li>'
}

function Change() {
    var sp = document.getElementById("STotalNot");
    var sp1 = document.getElementById("STotalNot1");
    $("#ImgNotify").attr("src", "../../images/notifyno.jpg");
    sp.style.display = "none";
    sp1.style.display = "none";
}
var contador = 0;
var contador2 = 0;

function msnPnotify(totalN) {
    contador++;
    if (contador == 100) {
    contador = 0;
    new Audio('../../Audio/Notify.mp3').play();
    new PNotify({
        title: 'Notificacion!!!',
        text: 'Usted tiene <span>' + totalN + '</span> alertas pendientes por leer',
        icon: 'glyphicon glyphicon-hand-right',
        animate: {
            animate: true,
            in_class: 'bounceInDown',
            out_class: 'hinge'
        }
    });

    }
}

function MarcarLeidoXAlarma(idAlarma) {

    transportNotify.executeRequest(
                transport.verb.post,
                'json',
                UrlApi + '/api/UsuarioAlarmasNotify?filtro=Alarma&idAlarma=' + idAlarma,
                     function (result) {
                         if (result != null) {

                         }
                         else {
                             bootbox.alert('No se pudo Marcar la alarma como leida.!');
                         }
                     }, function (jqXhr, textStatus) {
                         if (textStatus == "error") {

                         }
                     }
                );
};


function MarcarLeidoTodasAlarmas(IdUsuario) {

    transportNotify.executeRequest(
                transport.verb.post,
                'json',
                UrlApi + '/api/UsuarioAlarmasNotify?filtro=TotalAlarmas&IdUsuario=' + IdUsuario + '&segmento=' + segmentoN,
                     function (result) {
                         if (result == 200) {
                             location.reload(true);
                         }
                         else {
                             bootbox.alert('No se pudo Marcar la alarma como leida.!');
                         }
                     }, function (jqXhr, textStatus) {
                         if (textStatus == "error") {

                         }
                     }
                );
};

function confirmarCheckAll() {

    if ($('#CheckAll').is(':checked')) {

        bootbox.dialog({
            title: "Realmente desea marcar todos los mensajes como leidos?",
            message: '<div class="panel panel-default" id="myModal"  >'
                           + '<div class="panel-body">'
                                 + '<div class="col-md-12">'
                                     + '<form class="form-horizontal">'
                                         + '<fieldset>'
                                           + '<div class="form-group">'
                                               + '<div class="col-md-6">'
                                                   + '<button id="BtnGuardar" name="BtnGuardar" type="button" class="btn btn-primary center-block" onClick="ConfirmaSiCheckAll();">Confirmar</button>'
                                               + '</div>'
                                               + '<div class="col-md-6">'
                                                   + '<button id="BtnCancelar" name="BtnCancelar" type="button" class="btn btn-primary center-block">Cancelar</button>'
                                               + '</div>'
                                           + '</div>'
                                         + '</fieldset>'
                                       + '</form>'
                                 + '</div>'
                           + '</div>'
                     + '</div>',
            opacity: 0.5,
            width: "70%",
            height: "90%"
        });
        $('.bootbox-close-button').hide();
    }
    $("#BtnCancelar").click(ocultar);
};
function ConfirmaSiCheckAll() {
    bootbox.hideAll();
    MarcarLeidoTodasAlarmas(IdUsuario);
    $('#CheckAll').prop('checked', false);
    Change();
};

function ocultar() {
    bootbox.hideAll();
    $('#CheckAll').prop('checked', false);
};

