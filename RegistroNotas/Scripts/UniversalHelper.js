﻿/*----------

// Authors: Camilo Rodríguez
// Last Modification: 22/10/2014
// Version: 0.0.0.1

----------*/

var UniversalHelper = (function () {

    this.reloadPage = function () {
        window.location.reload();
    }

    this.cleanInputsAndSelcets = function (selectors) {
        for (var i = 0; i < selectors.length; i++) {
            $(selectors[i]).val('');
        }
    }
    this.cleanInputs = function (selectors) {
        for (var i = 0; i < selectors.length; i++) {
            $(selectors[i]).val('');
        }
    }

    this.cleanSelect = function (selectors) {
        for (var i = 0; i < selectors.length; i++) {
            $(selectors[i]).find('option').remove();
            console.log(selectors[i]);
        };
    }

    this.clearSelectList = function (list) {
        while(list.options.length) {
            list.remove(0);
        }
        }

    this.cleancheckbox = function (selectors) {
        for (var i = 0; i < selectors.length; i++) {
            $(selectors[i]).prop("checked", false);;
        }
    }
    this.InputsAndSelectsVacios = function (selectors) {
        var count = 0;
        for (i = 0; i < selectors.length; i++) {
            if ($(selectors[i]).val() == null || $(selectors[i]).val() == '' || $(selectors[i]).val() == ' ' || $(selectors[i]).val() == 0 || $(selectors[i]).val == "") {
                bootbox.alert("El campo " + $(selectors[i]).attr("name") + " no puede estar vacio.");
                return false;
            } else {
                count = count + 1;
            }
            if (count == selectors.length) {
                return true;
            }
        }
    }
    this.InputsAndSelectsVaciosBoleano = function (selectors) {
        var count = 0;
       // var estado = true;
        for (i = 0; i < selectors.length; i++) {
            if ($(selectors[i]).val() == null || $(selectors[i]).val() == '' || $(selectors[i]).val() == ' ' || $(selectors[i]).val() == 0 || $(selectors[i]).val == "") {
                //bootbox.alert("El campo " + $(selectors[i]).attr("name") + " no puede estar vacio.");
                //estado = false;
            } else {
                count = count + 1;
               // estado = true;
            }
        }
        if (count > 0) {
            return true;
        }
        else {
            return false;
        }
    }
    this.RadioCheckValidateGroup = function (selectors) {
        var count = 0;
        var estadoGroup = false;
        var cadena = '';
        for (i = 0; i < selectors.length; i++) {
            if ($(selectors[i]).val()) {
                count = count + 1;
                return estadoGroup;
            }
            cadena += '  ' + $(selectors[i]).attr("name");
        }
        if (!estadoGroup) {
            bootbox.alert('Debe seleccionar una opcion en ' + cadena);
            return estadoGroup
        }
    }
    this.SubSelectsVacios = function (selectors) {
        var count = 0;
        for (i = 0; i < selectors.length; i++) {
            if ($(selectors[i]).val() == 0) {
                bootbox.alert("El campo " + $(selectors[i]).attr("name") + " no puede estar vacio.");
                return false;
            } else {
                count = count + 1;
            }
            if (count == selectors.length) {
                return true;
            }
        }
    }
    this.cleanTable = function (selectorTable) {
        $(selectorTable).find('tbody>tr').remove();
    }

    //this.cleanSelect = function (selectorSelectTag) {
    //    $(selectorSelectTag).find('option').remove();
    //}

    this.showElements = function (selectors) {
        for (var i = 0; i < selectors.length; i++) {
            $(selectors[i]).show();
        }
    }

    this.hideElements = function (selectors) {
        for (var i = 0; i < selectors.length; i++) {
            $(selectors[i]).hide();
        }
    }
    this.enableElements = function (selectors) {
        for (var i = 0; i < selectors.length; i++) {
            $(selectors[i]).prop('disabled', false);
        }
    }

    this.disableElements = function (selectors) {
        for (var i = 0; i < selectors.length; i++) {
            $(selectors[i]).prop('disabled', true);
        }
    }

    this.removeOptionsByValue = function (selector) {
        for (var i = selector.length - 1; i >= 0; --i) {
            if (selector[i].value == 92 || selector[i].value == 93) {
                selector.remove(i);
            }
        }
    }

    this.addOptionsByValue = function (selector, newOptions) {
        for (var i in newOptions) {
            selector.append(new Option(newOptions[i], i));
        }
    }
    this.SelectOptionsByValue = function (selector) {
        selector.val('0').attr('selected', 'selected');
    }
    this.retornaCodigo = function (StringTexto) {
        var code = StringTexto.split(" -- ");
        return code[0];
    }
    this.Mayusculas = function (e, element) {
        key = (document.all) ? e.keyCode : e.which;
        element.value = element.value.toUpperCase();
    };
    this.tipos = function (tipo) {
        if (tipo == "Material" || tipo == "MATERIAL") {
            return false;
        }
        else {
            return true;
        }
    }

    this.slideOut = function (selector) {
        $(selector).animate({ "left": "-500px" }, { duration: 500, queue: false }, "easeOutExpo");
        $(selector).fadeOut(200);
    }

    this.slideIn = function (selector) {
        $(selector).css("left", "500px");

        $(selector).animate({ "left": "0px" }, { duration: 200, queue: false }, "easeOutExpo");
        $(selector).fadeIn(500);
    }
    this.navigateTo = function (data) {
        window.location.assign(data);
    }
    this.loaderAnimate = function () {
        $('body').prepend('<div id="loader-wrapper">'
                + '<div id="loader"></div>'
                + '</div> ');
    }
    this.loaderAnimateClose = function () {
        $('#loader-wrapper').remove();
    }

    this.ValidaSoloNumeros = function (e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = "0123456789";
        especiales = [8, 37, 39, 46];

        tecla_especial = false
        for (var i in especiales) {
            if (key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }

        if (letras.indexOf(tecla) == -1 && !tecla_especial)
            return false;
    }

    this.ValidaSololetra = function (e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = "abcdefghjklmnñopqrstuvwxyz";
        especiales = [8, 37, 39, 46];

        tecla_especial = false
        for (var i in especiales) {
            if (key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }

        if (letras.indexOf(tecla) == -1 && !tecla_especial)
            return false;
    }

    this.ValidaSoloNumerosandpunto = function (event) {
        if (((event.keyCode < 48) || (event.keyCode > 57)) && (event.keyCode != 46))
            event.returnValue = false;
    }
    this.storageAdd = function (Key, value) {
        if (window.localStorage) {

            sessionStorage.setItem(Key, value);
        }
        else {
            throw new Error('Tu Browser no soporta LocalStorage!');
        }
    }
    this.storageGet = function (Key) {
        if (window.localStorage) {
            return sessionStorage.getItem(Key);
        }
        else {
            throw new Error('Tu Browser no soporta LocalStorage!');
        }
    }
    this.storageRemove = function (Key) {
        if (window.localStorage) {
            sessionStorage.removeItem(Key);
        }
        else {
            throw new Error('Tu Browser no soporta LocalStorage!');
        }
    }

    this.getUserName = function (u, p, i) {
        transport.executeRequest(
            transport.verb.get,
            'jsonp',
            u,
            null,
            function (result) {
                if (result != null) {
                    $(p).text("");
                    $(p).text(result.name);
                } else {
                    $(p).text("");
                    $(p).text(i);
                }
            },
            function (jqXhr, textStatus) {
            }
        )
    }

    //funcion encargada de buscar un parametro del request
    this.getParameter = function (name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results == null) {
            return null;
        }
        else {
            return results[1] || 0;
        }
    }

    //validar solo numero positivos
    this.validaSoloNumerico = function (object) {
        var patron = /^\d+$/;
        if (!object.value.search(patron))
            return true;
        else
            bootbox.alert('Solo permite números');
        object.value = '';
        return false;
    }


    //validar solo numero positivos pro id
    this.validaSoloNumericoXid = function (selector) {
        var patron = /^\d+$/;
        if (!$(selector).val().search(patron))
            return true;
        else
            bootbox.alert('Solo permite números');
        $(selector).val('');
        return false;
    }


    this.CargarDatosMenuLocal = function () {
        var urlOrigen = window.location.origin;
        transport.executeRequest(
                     transport.verb.get,
                     "json",
                     urlOrigen + '/Menus/GetMenusList',
                     null,
                     function (resultCantM) {
                         UniversalHelper.CargarUrlsPermisos(resultCantM);
                     },
                         function (jqXhr, textStatus) {
                             console.log(textStatus);
                         }
                     );
    };

    this.CargarUrlsPermisos = function (resultCantM) {
        var LIstaUrlPermisos = [];
        if (resultCantM != null && resultCantM.length > 0) {

            /*For Controlador y vistas padre*/
            for (var id in resultCantM) {
                var objIndividual = resultCantM[id];
                var ItemActivo = objIndividual["_Active"];
                var Controlador = objIndividual["Controller"];
                var Vista = objIndividual["NomView"];
                if (ItemActivo == true && Controlador != null && Vista != null) {
                    LIstaUrlPermisos.push(Controlador.toUpperCase() + "/" + Vista.toUpperCase());
                } else {
                    var listAux = [];
                    listAux = objIndividual["ListMenu"];
                    for (var idList in listAux) {
                        objIndividual = listAux[idList];
                        ItemActivo = objIndividual["_Active"];
                        Controlador = objIndividual["Controller"];
                        Vista = objIndividual["NomView"];

                        if (ItemActivo == true && Controlador != null && Vista != null) {
                            LIstaUrlPermisos.push(Controlador.toUpperCase() + "/" + Vista.toUpperCase());
                        } else {

                            var listAux2 = [];
                            listAux2 = objIndividual["ListMenu"];
                            for (var idList2 in listAux2) {
                                objIndividual = listAux2[idList2];
                                ItemActivo = objIndividual["_Active"];
                                Controlador = objIndividual["Controller"];
                                Vista = objIndividual["NomView"];

                                if (ItemActivo == true && Controlador != null && Vista != null) {
                                    LIstaUrlPermisos.push(Controlador.toUpperCase() + "/" + Vista.toUpperCase());
                                }

                            }
                        }
                    }
                }
            }
        }
        else {
            LIstaUrlPermisos = null;
        }
        localStorage.setItem("ModelLocal", JSON.stringify(LIstaUrlPermisos));
    };

    this.ValidaMenus = function () {
        var Permiso = false;
        var valueLocalStorage = localStorage.getItem('ModelLocal');
        if (valueLocalStorage != null) {
            var ObjSplit = [];
            ObjSplit = valueLocalStorage.replace("[", "").replace("]", "").split(",");
            for (var item in ObjSplit) {
                if (/^([0-9])*$/.test(item)) {
                    var Obj = "/" + ObjSplit[item].replace("\"", "").replace("\"", "");
                    if (window.location.pathname.toUpperCase().includes(Obj.toUpperCase())) {
                        Permiso = true;
                        break;
                    }
                }

            };
        } else {

            UniversalHelper.CargarDatosMenuLocal;
            setTimeout(function () { UniversalHelper.ValidaMenus(); }, 3000);
        }

        if (!Permiso) {
            var urlOrigen = window.location.origin;
            var UrlDenied = window.location;
            var Ip = localStorage.getItem('Ip');
            var Name = localStorage.getItem('Name');
            var Response = null;
            transport.executeRequest(
                transport.verb.get,
                "json",
                urlOrigen + '/LogUrlDenied/CreaLog?Ip=' + Ip + '&UrlLog=' + UrlDenied,
                null,
                function (resultCantM) {
                    Response = resultCantM;
                    var Msg = '<div style="width: 20%; margin: auto;"><img src="../images/skullDanger.png" class="img-responsive" /> </div>'
                        + '<spam style="font-style: italic; color: black; font-size: inherit">' + Name + '</spam>' + ' Usted está intentado vulnerar la seguridad del sistema, es la ' + '<spam class="alert-danger" style="font-size: large;">' + Response + '</spam>' + ' vez que ocurre, por tal motivo esta situación será informada a sistemas.';
                    bootbox.dialog({
                        title: '<div class="alert alert-danger" role="alert"><span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span><span class="sr-only" style=></span> Alerta de seguridad.</div>',
                        message: '<div class="panel panel-default">'
                                    + '<div class="panel-body">'
                                        + '<div class="form-group">'
                                            + '<div class="alert alert-warning" role="alert">'
                                            + Msg
                                            + '</div>'
                                        + '</div>'
                                        + '<div id="divMasterTwo"  class="form-group">'
                                              + '<div  class="col-md-6" style="width: 100%;">'
                                                    + '<a type="button" class="btn btn-primary center-block" id="BtnSalida" href="javascript:window.location.href=\'/Account/cerrar\'">Aceptar</a>'
                                              + '</div>'
                                        + '</div>'
                                    + '</div>'
                                 + '</div>',
                        opacity: 0.5,
                        width: "50%",
                        height: "30%"
                    });
                    $('.bootbox-close-button').hide();
                },
                function (jqXhr, textStatus) {
                    console.log(textStatus);
                }
            );

        }

    };
    this.DatosUsuario = function (cedula, segmento, callback) {
        $.ajax({
            method: 'POST',
            url: '/Account/Usuario',
            dataType: 'json',
            data: { cedula: cedula, segmento: segmento },
            success: function (data) {
                callback(null, data);
            },
            error: function (jqXHR) { callback(jqXHR); }
        });
    }
    this.GPSWatcher = function () {
        options = {
            enableHighAccuracy: false,
            timeout: 5000,
            maximumAge: 0
        };

        if ("geolocation" in navigator) {
            /* geolocation is available */
            var watchID = navigator.geolocation.watchPosition(function (position) {
                $(document).trigger("GPSPositionChanged", [position.coords.latitude, position.coords.longitude, position.coords.accuracy]);
            }, function (err) {
                $(document).trigger('GPSMessage', 'Usted debe habilitar el GPS para usar esta aplicación.');
            }, options);
        } else {
            /* geolocation IS NOT available */
            $(document).trigger('GPSMessage', 'Su dispositivo no soporta GPS, por lo tanto no podrá usar esta aplicación.');
        }
    }

    this.formatNumber = {
        separador: ".", // separador para los miles
        sepDecimal: ',', // separador para los decimales
        formatear: function (num) {
            num += '';
            var splitStr = num.split('.');
            var splitLeft = splitStr[0];
            var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
            var regx = /(\d+)(\d{3})/;
            while (regx.test(splitLeft)) {
                splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
            }
            return this.simbol + splitLeft + splitRight;
        },
        new: function (num, simbol) {
            this.simbol = simbol || '';
            return this.formatear(num);
        }
    }

    return (this);

}).call({});