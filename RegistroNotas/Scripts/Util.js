﻿
function confirm(header, message, callback) {
    $('#confirm').modal({
        closeHTML: "<a href='#' title='Close' class='modal-close'>x</a>",
        position: ["20%", ],
        overlayId: 'confirm-overlay',
        containerId: 'confirm-container',
        onShow: function (dialog) {
            var modal = this;

            $('.message', dialog.data[0]).append(message);
            $('.header span', dialog.data[0]).append(header);

            // if the user clicks "yes"
            $('.yes', dialog.data[0]).click(function () {
                // call the callback
                if ($.isFunction(callback)) {
                    callback.apply();
                }
                // close the dialog
                modal.close(); // or $.modal.close();
            });
        }
    });
}

function mensaje(header, message) {
    $('#mensaje').modal({
        closeHTML: "<a href='#' title='Close' class='modal-close'>x</a>",
        position: ["20%", ],
        overlayId: 'confirm-overlay',
        containerId: 'confirm-container',
        onShow: function (dialog) {
            $('.message', dialog.data[0]).append(message);
            $('.header span', dialog.data[0]).append(header);
        }
    });
}

function validarContrasena(contrasena, cabecera, textoalerta) {
    expr = /(?=^.{8,}$)(?=.*\d)(?=.*\W+)(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;
    if (!contrasena.match(expr))
        mensaje(cabecera, textoalerta);
    return false;
}

function ValidaSoloNumeros() {
    if ((event.keyCode < 48) || (event.keyCode > 57))
        event.returnValue = false;
}

function SoloNumeros(e) {
    var key = window.Event ? e.which : e.keyCode
    return (key >= 48 && key <= 57)
}


function ValidaSololetrasM(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
    especiales = "8-37-39-46";

    tecla_especial = false
    for (var i in especiales) {
        if (key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }
    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
        return false;
    }
}

function soloLetras(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
    especiales = "8-37-39-46";

    tecla_especial = false
    for (var i in especiales) {
        if (key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }
    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
        return false;
    }
}

function soloLetrassinespacio(e) {
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = "áéíóúabcdefghijklmnñopqrstuvwxyz";
    especiales = "8-37-39-46";

    tecla_especial = false
    for (var i in especiales) {
        if (key == especiales[i]) {
            tecla_especial = true;
            break;
        }
    }

    if (letras.indexOf(tecla) == -1 && !tecla_especial) {
        return false;
    }
}

function LimpiarControl(listacontroles) {
    var controles = listacontroles.split(",");

    for (i = 0; i < controles.length; i++) {
        $("#" + controles[i]).val(null);
    }
}

function ValidarControlVacio(listacontroles) {
    var controles = listacontroles.split(",");
    var count = 0;
    for (i = 0; i < controles.length; i++) {
        if ($("#" + controles[i]).val() == null || $("#" + controles[i]).val() == '' || $("#" + controles[i]).val() == ' ') {
            alert("El campo " + $("#" + controles[i]).attr("name") + " no puede estar vacio.");
            return false;
        } else {
            count = count + 1;
        }
        if (count == controles.length) {
            return true;
        }
    }
}

function Datafilter(items) {
    var controles = items.split(",");
    if (items != 0) {

    }
}

function ValidaFecha(fecini, fecfin) {
    debugger;
    var fechaInicial = moment(inicio = $("#" + fecini).val(), "DD-MM-YYYY");
    var fechaFinal = moment($("#" + fecfin).val(), "DD-MM-YYYY");

    if (moment(fechaFinal).isBefore(fechaInicial) || moment(fechaInicial).isSame(fechaFinal)) {
        return true;
    } else {
        return false;
    }
}

function ValidaFecha2(fecini, fecfin) {
    debugger;
    var fechaInicial = moment(inicio = $("#" + fecini).val(), "DD-MM-YYYY");
    var fechaFinal = moment($("#" + fecfin).val(), "DD-MM-YYYY");

    if (moment(fechaFinal).isBefore(fechaInicial)) {
        return true;
    } else {
        return false;
    }
}

function filesup(item) {
    var url3 = '@ViewBag.UrlApiService/api/GestionFactibilidad' + item.val();
    http2.open("GET", url2, true);
    http2.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    http3.onreadystatechange = function () {
        if (http3.readyState == 4 && http3.status == 200) {
            var json3 = JSON.parse(http3.response);
            $("#listfile").append("<li> <a href=" + "<li>")

        }
        if (http3.readyState == 4 && http3.status == 400) {
            alert(http3.response);
        }
    }
}

function validarRadio(F) {
    var i
    for (i = 0; i < F.radioSiNo.length; i++) {
        if (F.radioSiNo[i].checked) {
            return true;
        }
        else {
            return false;
        }

    }
}

function Mayusculas(e, element) {
    key = (document.all) ? e.keyCode : e.which;
    element.value = element.value.toUpperCase();
};

function ControlSDPlacas(e, element) {
    if (element.value.match(expr))
        return false;
    return true;
}

function ControlPlacas(campo) {
    //       /^\d{2}\/\d{2}\/\d{4}$
    var RegExPattern = /^([A-Z]{3}\-\d{3})$/;
    var errorMessage = 'Incorrecta.';
    if ((campo.match(RegExPattern)) && (campo != '')) {
        alert('Correcta');
    } else {
        alert(errorMessage);
        campo.focus();
    }
}

function fechaActual() {
    var f = new Date();

    //document.write = (f.getDate() + "/" + (f.getMonth() + 2) + "/" + f.getFullYear());
    return (f.getDate() + "/" + (f.getMonth() + 2) + "/" + f.getFullYear());
}

function CambiaX(x) {
    var textReturn = "";
    if (x.Equals("True"))
    { textReturn = "X"; }
    else
    { textReturn = " "; }
    return textReturn;
}

function showPosition(position) {
    latitude = position.coords.latitude;
    longitude = position.coords.longitude;
    img_url = "http://maps.googleapis.com/maps/api/staticmap?center=" + latitude + "," + longitude + "&zoom=14&size=600x300&maptype=roadmap&markers=color:blue%7C" + latitude + "," + longitude + "&sensor=false";

    $("#mapa").attr('src', img_url);
    $("#GlobalLatitud").val(latitude);
    $("#GlobalLongitud").val(longitude);

    function showError(error) {
        switch (error.code) {
            case error.PERMISSION_DENIED:
                alert("El usuario denego la solicitud para geolocalización.");
                break;
            case error.POSITION_UNAVAILABLE:
                alert("La información de localización no esta disponible.");
                break;
            case error.TIMEOUT:
                alert("Solicitud fuera de tiempo.");
                break;
            case error.UNKNOWN_ERROR:
                alert("Error desconocido.");
                break;
        }
    }
}


